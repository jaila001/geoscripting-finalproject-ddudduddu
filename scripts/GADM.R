# Team: DduDduDdu
# Intan Ika Apriani & Zakiul Fahmi Jailani
# Date: 01.02.2019
# Final Project
# Description: Get GADM

getGADM <- function(country, district){
  country = country
  lvl = 4 # Using level 4 to get sub-district information
  
  #Convert the country name to a countrycode so the data can be downloaded
  countrycode <- ccodes()[ccodes()["NAME"] == country][2]
  
  #Gets the administrative data for the given country and level
  adm <- raster::getData("GADM", country = countrycode, level = lvl, path = "Data")
  
  # Margin definition based on administration
  return(adm[adm$NAME_2 == district,]) # Palu is the name of the district
  
  
  
}


